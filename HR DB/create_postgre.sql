create table HR_EMPLOYEE (
                             ID_ serial,
                             FAMILY_NAME_ varchar(100) not null,
                             FIRST_NAME_ varchar(50) not null,
                             SECOND_NAME_ varchar(50),
                             UNIT_ID_ integer not null,
                             POSITION_ID_ integer not null,
                             SALARY_ integer not null,
                             DATE_HIRED_ timestamp not null,
                             DATE_FIRED_ timestamp,
                             primary key (ID_)
);

create table HR_UNIT (
                         ID_ serial,
                         NAME_ varchar(400) not null,
                         HEAD_ integer,
                         primary key (ID_)
);

create table HR_POSITION (
                             ID_ serial,
                             NAME_ varchar(400) not null,
                             primary key (ID_)
);

create table HR_VACATION (
                             ID_ serial,
                             EMPLOYEE_ID_ integer,
                             DATE_START_ timestamp not null,
                             DATE_END_ timestamp not null,
                             TYPE_ integer,
                             primary key (ID_)
);

create table HR_VACATION_TYPE (
                                  ID_ serial,
                                  NAME_ varchar(50) not null,
                                  primary key (ID_)
);

create index HR_IDX_EMPLOYEE_UNIT on HR_EMPLOYEE(UNIT_ID_);
alter table HR_EMPLOYEE
    add constraint HR_FK_EMPLOYEE_UNIT
        foreign key (UNIT_ID_)
            references HR_UNIT (ID_);
alter table HR_EMPLOYEE
    add constraint HR_FK_EMPLOYEE_POSITION
        foreign key (POSITION_ID_)
            references HR_POSITION (ID_);

create index HR_IDX_UNIT_HEAD on HR_UNIT(HEAD_);
alter table HR_UNIT
    add constraint HR_FK_UNIT_HEAD
        foreign key (HEAD_)
            references HR_EMPLOYEE (ID_);

create index HR_IDX_VACATION_START on HR_VACATION(DATE_START_);
create index HR_IDX_VACATION_END on HR_VACATION(DATE_END_);
create index HR_IDX_VACATION_EMPLOYEE on HR_VACATION(EMPLOYEE_ID_);
alter table HR_VACATION
    add constraint HR_FK_VACATION_EMPLOYEE
        foreign key (EMPLOYEE_ID_)
            references HR_EMPLOYEE (ID_);
alter table HR_VACATION
    add constraint HR_FK_VACATION_TYPE
        foreign key (TYPE_)
            references HR_VACATION_TYPE (ID_);